﻿using MorpionGame;

namespace MorpionClientCommon
{
    /// <summary>
    /// C'est un controlleur, cote client, qui permet de gerer le lien entre les données d'un serveur 
    /// et le GUI pour un match donne
    /// </summary>
    public class SFSMatchController : IMatchController
    {
        Match _match = null;
        SFSServer _server = null;

        /// <summary>
        /// le constructeur est internal comme les instance ne sont cree que par le SFSServer quand les 
        /// conditions le permettent(etre dans une room sur le serveur et avoir recu l'etat du match)
        /// </summary>
        /// <param name="match"></param>
        /// <param name="server"></param>
        internal SFSMatchController(IMatchListener match, SFSServer server)
        {
            _match = match as Match;
            _server = server;
            _server.OnPlay += OnPlay; 
        }

        private void OnPlay(int cell)
        {
            _match.Play(cell);
        }

        public IMatchListener GetMatch()
        {
            return _match;
        }

        public void Play(int cell)
        {
            _server.Play(cell, _match.CurrentPlayer);
        }

        public void Leave()
        {
            _server.LeaveMatch(_match.GetName());
            _server.OnPlay -= OnPlay;
        }
    }
}
