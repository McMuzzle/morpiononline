﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System;

using Sfs2X;
using Sfs2X.Core;
using Sfs2X.Entities;
using Sfs2X.Entities.Data;

namespace MorpionClientCommon
{
    /// <summary>
    /// Classe qui permet de rejoindre un serveur Smartfox qui heberge des parties de morpion et de
    /// communiquer
    /// </summary>
    public class SFSServer
    {
        /// <summary>
        /// Un event qui indique que l'etat de la connection a change
        /// </summary>
        /// <param name="ready">true si on est pret a communiquer, false dans les autres cas</param>
        public delegate void OnConnectionChangeAction(bool ready);
        public event OnConnectionChangeAction OnConnectionChange = delegate { };

        /// <summary>
        /// Event qui indique qu'on a recu une demande du serveur de jouer un certain coup
        /// </summary>
        /// <param name="cell"></param>
        public delegate void OnPlayAction(int cell);
        public event OnPlayAction OnPlay = delegate { };

        /// <summary> dictionnaire qui contient un CB pour chaque requete envoyé au serveur sur qui doit traiter la valeur de retour</summary>
        private Dictionary<int, Action<ISFSObject>> _callbacks = new Dictionary<int, Action<ISFSObject>>();

        /// <summary> les match en cours d'attente d'info du serveur, quand on recoit l'info on trigger l'action</summary>
        private Dictionary<string, Action<string>> _matchAwaited = new Dictionary<string, Action<string>>();
        
        /// <summary> The host to connect to </summary>
        private string _host = "51.254.200.78";

        /// <summary> the username the connection will use to log in the server </summary>
        private string _username = "";

        public string Username
        {
            get {
                return _username;
            }
            set
            {
                if (_username != value)
                {
                    _username = value;
                    Connect();
                }
            }
        }

        private SmartFox _smartfox = null;
        private int _reqID = 1; //un compteur qui permet d'identifier les requete qu'on envoie au serveur d'une facon unique
        private Room _currentMatch = null;

        public SFSServer(string host, string username)
        {
            _host = host;
            _username = username;

            Connect();
        }

        public bool Ready { get
            {
                return _smartfox != null && _smartfox.IsConnected && _smartfox.JoinedRooms.Count > 0;
            } 
        }

        /// <summary>
        /// Requete qui permet de recuperer les matchs du serveur
        /// </summary>
        /// <returns></returns>
        public Task<List<MorpionGame.IMatchListener>> GetMatchs()
        {
            List<MorpionGame.IMatchListener> result = null;

            SFSObject data = new SFSObject();
            data.PutInt("id", _reqID++);
            var req = new Sfs2X.Requests.ExtensionRequest("GetMatchs", data);

            //on indique quoi faire quand on recoit la reponse du serveur
            _callbacks.Add(data.GetInt("id"), (data) => {
                List<MorpionGame.IMatchListener> tmpResult = new List<MorpionGame.IMatchListener>();
                foreach(ISFSObject m in data.GetSFSArray("matchs"))
                {
                    string json = m.GetUtfString("json");
                    if (json == "")
                    {
                        MorpionGame.Match match = new MorpionGame.Match(m.GetUtfString("name"));
                        string p1 = m.GetUtfString("player1");
                        string p2 = m.GetUtfString("player2");
                        match.Player1 = p1 == "" ? null : p1;
                        match.Player2 = p2 == "" ? null : p2;
                        tmpResult.Add(match);
                    }
                    else
                    {
                        MorpionGame.IMatchListener match = MorpionGame.MatchFactory.MatchFromJSON(json);
                        tmpResult.Add(match);
                    }                    
                }
                result = tmpResult;
            });

            _smartfox.Send(req);

            //attendre la réponse ou le timeout
            float timeout = 10.0f;
            float elapsed = 0.0f;
            while(elapsed < timeout)
            {
                Task.Delay(100).Wait();
                elapsed += 0.1f;
                if (null != result)
                    break;
            }


            if (null == result)
            {
                //timeout!
            }


            return Task.FromResult(result);
        }

        public void CreateMatch()
        {
            ISFSObject data = new SFSObject();
            var req = new Sfs2X.Requests.ExtensionRequest("CreateMatch", data, null);
            _smartfox.Send(req);
        }

        public void JoinMatch(string matchName)
        {
            ISFSObject data = new SFSObject();
            data.PutUtfString("matchname", matchName);
            var req = new Sfs2X.Requests.ExtensionRequest("JoinMatch", data, null);
            _smartfox.Send(req);
        }

        /// <summary>
        /// permet de rejoindre un match, pour ce faire le serveur nous place dans la room du match,
        /// on recoit l'etat du match et on peut retourner un SFSMatchController
        /// </summary>
        /// <param name="matchName"></param>
        /// <param name="callback"></param>
        public void EnterMatch(string matchName, Action<SFSMatchController> callback)
        {
            SFSMatchController result = null;

            SFSObject data = new SFSObject();
            data.PutInt("id", _reqID++);
            data.PutUtfString("matchName", matchName);

            _matchAwaited.Add(matchName, (json) =>
             {
                 var match = MorpionGame.MatchFactory.MatchFromJSON(json);
                 result = new SFSMatchController(match, this);
             });

            var req = new Sfs2X.Requests.ExtensionRequest("JoinMatchRoom", data);
            _smartfox.Send(req);

            //ici on attend d'avoir recu le contenu du match (todo : ajouter un timeout)
            Task.Run(() =>
            {
                while (result == null)
                {
                    Task.Delay(100).Wait();                
                }                
                callback(result);
            });
        }

        /// <summary>
        /// indiquer au serveur qu'on quitte une table
        /// </summary>
        /// <param name="matchName"></param>
        public void LeaveMatch(string matchName)
        {
            _smartfox.Send(new Sfs2X.Requests.LeaveRoomRequest(_currentMatch));
        }

        /// <summary>
        /// envoyer au serveur une demande pour jouer un coup
        /// </summary>
        /// <param name="cell"></param>
        /// <param name="state"></param>
        public void Play(int cell, int state)
        {
            
            SFSObject data = new SFSObject();
            data.PutInt("id", _reqID++);
            data.PutInt("cell", cell);
            data.PutInt("state", state);

            var req = new Sfs2X.Requests.ExtensionRequest("Play", data, _currentMatch);

            _smartfox.Send(req);
        }

        /// <summary>
        /// Demander une connection au serveur, terminer la connection précédente si elle existe
        /// </summary>
        private void Connect()
        {
            if (_username == "")
                return;
            if(null != _smartfox)
            {
                _smartfox.Disconnect();
                _smartfox = null;
            }

            _smartfox = new SmartFox();

            Sfs2X.Util.ConfigData config = new Sfs2X.Util.ConfigData();
            config.Host = _host;
            config.Debug = true;
            config.Zone = "Morpion";

            _smartfox.AddEventListener(SFSEvent.CONNECTION, OnConnect);
            _smartfox.AddEventListener(SFSEvent.LOGIN, OnLogin);
            _smartfox.AddEventListener(SFSEvent.LOGIN_ERROR, OnLoginError);
            _smartfox.AddEventListener(SFSEvent.ROOM_JOIN, OnRoomJoin);
            _smartfox.AddEventListener(SFSEvent.CONNECTION_LOST, OnConnectionLost);
            _smartfox.AddEventListener(SFSEvent.PUBLIC_MESSAGE, OnMessage);
            _smartfox.AddEventListener(SFSEvent.EXTENSION_RESPONSE, OnExtensionResponse);

            _smartfox.Connect(config);
        }

        /// <summary>
        /// doit etre appelé regulierement pour que les messages soient envoyes et recu avec le serveur
        /// </summary>
        public void Update()
        {
            if(null != _smartfox)
                _smartfox.ProcessEvents();
        }

        /// <summary>
        /// Callback appele quand on se connecte au serveur
        /// </summary>
        /// <param name="evt"></param>
        protected void OnConnect(BaseEvent evt)
        {
            bool success = (bool)evt.Params["success"];
            Console.WriteLine("On Connection callback got: " + success);           

            if (success)
            {
                _smartfox.Send(new Sfs2X.Requests.LoginRequest(_username));
            } else
            {
                Connect();
            }
        }

        /// <summary>
        /// Callback appele quand le login est effectue
        /// </summary>
        /// <param name="evt"></param>
        protected void OnLogin(BaseEvent evt)
        {
            Console.WriteLine("Login success");
            _smartfox.Send(new Sfs2X.Requests.JoinRoomRequest("The Lobby", "", null, false));
        }

        /// <summary>
        /// Callback appele quand on rejoin une room sur le serveur 
        /// les rooms permettent de diviser les messages et eviter d'envoyer tout a tout le monde
        /// </summary>
        /// <param name="evt"></param>
        protected void OnRoomJoin(BaseEvent evt)
        {
            Room room = (Room)evt.Params["room"];
            if(room.Name == "The Lobby")
            {
                OnConnectionChange(true);
            } else if(room.GroupId == "match")
            {
                _currentMatch = room;
            }
            
        }

        /// <summary>
        /// Callback appele si le login a echoué
        /// </summary>
        /// <param name="evt"></param>
        protected void OnLoginError(BaseEvent evt)
        {
            string message = (string)evt.Params["errorMessage"];
            Console.WriteLine("Error login : " + message);
        }

        /// <summary>
        /// Callback appelé quand la connection est perdue
        /// </summary>
        /// <param name="evt"></param>
        protected void OnConnectionLost(BaseEvent evt)
        {
            OnConnectionChange(false);
            Connect();
        }

        /// <summary>
        /// Callback appele chaque fois qu'un message (chat message) arrive dans une room
        /// </summary>
        /// <param name="evt"></param>
        public void OnMessage(BaseEvent evt)
        {
            var room = (Room)evt.Params["room"];
            var user = (User)evt.Params["sender"];
            string message = (string)evt.Params["message"];
            var data = (ISFSObject)evt.Params["data"];

            Console.WriteLine("Message(" + room.Name + ") : " + message);
        }

        /// <summary>
        /// Callback appele quand le serveur envoie un message depuis un code d'extension
        /// </summary>
        /// <param name="evt"></param>
        public void OnExtensionResponse(BaseEvent evt)
        {
            string cmd = (string)evt.Params["cmd"];
            Room room = null;
            if (evt.Params.ContainsKey("room")) {
                room = (Room)evt.Params["room"];
            }
            
            
            ISFSObject data = (ISFSObject)evt.Params["params"];
            int id = data.GetInt("id");
            if (_callbacks.ContainsKey(id))
            {
                _callbacks[id].Invoke(data);
            }

            switch (cmd)
            {
                case "PlayConfirmation":
                    {
                        int cell = data.GetInt("cell");
                        OnPlay(cell);
                    }break;
                case "matchData":
                    {
                        string json = data.GetUtfString("json");
                        string match = data.GetUtfString("name");
                        if (_matchAwaited.ContainsKey(match))
                        {
                            _matchAwaited[match](json);
                            _matchAwaited.Remove(match);
                        }
                    }break;
            }
        }
    }
}
