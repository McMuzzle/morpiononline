﻿using System;

namespace MorpionGame
{
    public enum TileState
    {
        Empty,
        X,
        O
    };

    /// <summary>
    /// C'est une partie de Morpion au sens donné du terme. 
    /// </summary>
    internal class Match : IMatchListener
    {

        [Newtonsoft.Json.JsonProperty]
        private int[] _cells = new int[9];  //cell empty = 0, X = 1, O = -1
        [Newtonsoft.Json.JsonProperty]
        private int _currentPlayer = 0; //nobody 0, X = 1, Y = -1
        [Newtonsoft.Json.JsonProperty]
        private int _winner = 0;

        public string Name { get; private set;}
        public string Player1 { get; set; }
        public string Player2 { get; set; }

        public event IMatchListener.MatchChangeAction OnMatchChange;

        public Match()
        {
            Name = "DefaultName";
            _currentPlayer = 1;
        }

        public Match(string name)
        {
            Name = name;
            _currentPlayer = 1;
        }

        public bool Finished()
        {
            return _currentPlayer == 0;
        }

        public TileState Winner()
        {
            switch (_winner)
            {
                case 1: return TileState.X;
                case -1: return TileState.O;
            }
            return TileState.Empty;
        }

        public string GetName()
        {
            return Name;
        }
        
        [Newtonsoft.Json.JsonIgnore]
        public int CurrentPlayer
        {
            get { return _currentPlayer; }
        }

        public TileState GetTileState(int index)
        {
            if (index < 0 || index >= _cells.Length)
                throw new Exception("Index out of bound in GettileState");

            switch (_cells[index])
            {
                case 0: return TileState.Empty;
                case 1: return TileState.X;
                case -1: return TileState.O;
            }

            throw new System.Exception("wrong value for cell " + index + " : " + _cells[index]);
        }

        #region Actions
        public void Play(int cell)
        {
            if (_currentPlayer == 0)
                throw new Exception("Finished game");
            if (cell < 0 || cell >= _cells.Length)
                throw new Exception("Index out of board " + cell);
            if (_cells[cell] != 0)
                throw new Exception("Index allready used " + cell);

            _cells[cell] = _currentPlayer;
            _currentPlayer = -_currentPlayer;
          
            CheckEndGame();

            OnMatchChange?.Invoke();
        }

        public bool CanPlayCell(int cell)
        {
            if (cell < 0 || cell >= _cells.Length)
                throw new Exception("Index out of board " + cell);

            return _cells[cell] == 0 && !Finished();
        }

        #endregion

        #region logic
        private void CheckEndGame()
        {
            //tester lignes
            for(int x = 0; x < 3; x++)
            {
                if(_cells[x * 3] != 0 &&_cells[x*3] == _cells[x * 3 + 1] && _cells[x * 3 + 1] == _cells[x * 3 + 2])
                {
                    SetWinner(_cells[x * 3]);
                    break;
                }
                if (_cells[x] != 0 && _cells[x] == _cells[x + 3] && _cells[x + 3] == _cells[x + 6])
                {
                    SetWinner(_cells[x]);
                    break;
                }
            }
            //diagonales
            if (_cells[0] != 0 &&_cells[0] == _cells[4] && _cells[4] == _cells[8])
                SetWinner(_cells[0]);

            if (_cells[2] != 0 && _cells[2] == _cells[4] && _cells[4] == _cells[6])
                SetWinner(_cells[2]);

            //test full
            if (!Finished())
            {
                bool free = false;
                foreach (var cell in _cells)
                {
                    if (cell == 0)
                    {
                        free = true;
                        break;
                    }
                }
                if (!free)
                {
                    _currentPlayer = 0;
                }
            }
        }
        private void SetWinner(int winner)
        {
            _currentPlayer = 0;
            _winner = winner;
        }
        #endregion

        #region saveManagement

        public static Match Deserialize(string data)
        {
            Match result = Newtonsoft.Json.JsonConvert.DeserializeObject<Match>(data);          
            return result;
        }

        public string Serialize()
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(this);
        }

        #endregion
    }
}
