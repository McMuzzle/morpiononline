﻿namespace MorpionGame
{
    public interface IMatchListener
    {
        public delegate void MatchChangeAction();
        public event MatchChangeAction OnMatchChange;

        public TileState GetTileState(int index);

        /// <summary>
        /// permet de savoir s'il est presentement légal de jouer dans la case en question
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public bool CanPlayCell(int index);

        /// <summary>
        /// retourne vrai si le match est termine
        /// </summary>
        /// <returns></returns>
        public bool Finished();

        /// <summary>
        /// Si le match est termine, retourne le gagnant ou empty si c'est nul
        /// </summary>
        /// <returns></returns>
        public TileState Winner();

        /// <summary>
        /// permet de recuperer le nom (unique) du match en question
        /// </summary>
        /// <returns></returns>
        public string GetName();

        public string Player1 { get; }

        public string Player2 { get; }
    }
}
