﻿using System.Windows.Input;

namespace MorpionGame
{
    public interface IMatchController
    {
        /// <summary>
        /// demander au controller de jouer un coup a la cellule donnée
        /// </summary>
        /// <param name="cell"></param>      
        public void Play(int cell);

        /// <summary>
        /// Permet de recuperer un match en lecture seule
        /// </summary>
        /// <returns></returns>
        public IMatchListener GetMatch();

        //we leave a match
        public void Leave();
    }
}
