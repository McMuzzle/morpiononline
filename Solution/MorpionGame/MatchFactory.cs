﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MorpionGame
{
    public static class MatchFactory
    {
        public static IMatchListener MatchFromJSON(string json)
        {
            Console.WriteLine(json);
            return Match.Deserialize(json);
        }
    }
}
