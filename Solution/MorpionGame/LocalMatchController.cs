﻿namespace MorpionGame
{
    public class LocalMatchController : IMatchController
    {
        /// <summary> le match a gerer </summary>
        Match _match = null;

        public LocalMatchController()
        {
            _match = new Match("LocalMatch");
        }

        public void Play(int cell)
        {
            _match.Play(cell);
        }

        public IMatchListener GetMatch()
        {
            return _match;
        }

        public void Leave()
        {
        }
    }
}
