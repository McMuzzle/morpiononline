﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MorpionPortable
{
     /// <summary>
     /// C'est l'application qui permet de jouer au morpion sur android et IOS
     /// </summary>
    public partial class App : Application
    {
        MorpionClientCommon.SFSServer _server = null;

        public App()
        {
            string username = "Client";
            if (Application.Current.Properties.ContainsKey("username"))
            {
                username = Application.Current.Properties["username"] as string;
            } 

            _server = new MorpionClientCommon.SFSServer("51.254.200.78", username);

            Device.StartTimer(TimeSpan.FromMilliseconds(10), Loop);

            InitializeComponent();
            
            MainPage = new NavigationPage(new MainPage(_server));            
        }

        private bool Loop()
        {
            _server.Update();
            return true;
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
