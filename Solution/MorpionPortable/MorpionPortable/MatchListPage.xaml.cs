﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using MorpionClientCommon;

namespace MorpionPortable
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MatchListPage : ContentPage
    {
        SFSServer _server = null;

        List<MorpionGame.IMatchListener> _matchs = null;
        bool _loading = false;

        public MatchListPage(SFSServer server)
        {
            InitializeComponent();

            _server = server;


            Task.Run(GetMatchs);
        }

        void GetMatchs()
        {
            Xamarin.Essentials.MainThread.BeginInvokeOnMainThread(() =>
            {
                SetMatchList(new List<MorpionGame.IMatchListener>());
            });
            var a = _server.GetMatchs();
            SetLoading(true);
            a.Wait();
            Xamarin.Essentials.MainThread.BeginInvokeOnMainThread(() =>
            {
                SetMatchList(a.Result);
            });
        }

        private void SetLoading(bool isLoading)
        {
            _loading = isLoading;
        }

        private void SetMatchList(List<MorpionGame.IMatchListener> matchsList)
        {
            SetLoading(false);

            _matchs = matchsList;

            matchs.ItemsSource = _matchs;
        }

        private void OnCreatemMatch(object sender, EventArgs e)
        {
            _server.CreateMatch();

            Task.Run(GetMatchs);
        }

        private void OnSelect(object sender, EventArgs e)
        {
            
            ItemTappedEventArgs castE = e as ItemTappedEventArgs;
            MorpionGame.IMatchListener match = castE.Item as MorpionGame.IMatchListener;
            if (match.Player1 != null && match.Player1 != "" && match.Player2 != null && match.Player2 != "")
            {
                _server.EnterMatch(match.GetName(), (controller) =>
                {
                    Xamarin.Essentials.MainThread.BeginInvokeOnMainThread(() =>
                    {
                        Navigation.PushAsync(new MatchPage(controller));
                    });
                });
            } else
            {
                _server.JoinMatch(match.GetName());

                Task.Run(GetMatchs);
            }
        }
    }
}