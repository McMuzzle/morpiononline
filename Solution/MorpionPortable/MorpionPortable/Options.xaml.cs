﻿using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MorpionPortable
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Options : ContentPage
    {
        MorpionClientCommon.SFSServer _server = null;

        public Options(MorpionClientCommon.SFSServer server)
        {
            _server = server;
            InitializeComponent();
        }
        
        async void validate(object sender, EventArgs e)
        {
            _server.Username = usernameBox.Text;

            Application.Current.Properties["username"] = _server.Username;

            await Navigation.PopAsync();
        }
    }
}