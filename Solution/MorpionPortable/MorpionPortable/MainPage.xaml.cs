﻿using System;
using Xamarin.Forms;

namespace MorpionPortable
{
    public partial class MainPage : ContentPage
    {
        MorpionClientCommon.SFSServer _server = null;

        public MainPage(MorpionClientCommon.SFSServer server)
        {
            _server = server;

            InitializeComponent();

            Server_OnConnectionChange(_server.Ready);

            server.OnConnectionChange += Server_OnConnectionChange;
        }

        async private void Server_OnConnectionChange(bool ready)
        {
            ButtonOnline.IsEnabled = ready;

            if (!ready)
            {
                while (Navigation.NavigationStack.Count > 1)
                {
                    await Navigation.PopAsync();
                }
            }
        }

        async void PlayLocal(object sender, EventArgs e)
        {            
            MorpionGame.IMatchController controller = new MorpionGame.LocalMatchController();

            await Navigation.PushAsync(new MatchPage(controller));
        }

        async void PlayOnline(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new MatchListPage(_server));
        }

        async void Options(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Options(_server));
        }
    }
}
