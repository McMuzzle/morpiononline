﻿using System.Windows.Input;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using MorpionGame;

namespace MorpionPortable
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MatchPage : ContentPage
    {
        private IMatchController _matchCtrl = null;
        private IMatchListener _match = null;

        public ICommand Play { get; set; }       

        public MatchPage(IMatchController matchCtl)
        {
            Play = new Command<string>(PlayAt);

            _matchCtrl = matchCtl;
            _match = matchCtl.GetMatch();

            _match.OnMatchChange += OnMatchChange;
            
            InitializeComponent();
            BindingContext = this;

            OnMatchChange();
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            _matchCtrl.Leave();
            return;
        }

        private void PlayAt(string cell)
        {
            int cellInt = int.Parse(cell);

            if (_match.CanPlayCell(cellInt))
            {
                _matchCtrl.Play(cellInt);
            }
        }

        private void OnMatchChange()
        {
            for(int i = 0; i < 9; i++)
            {
                Image image = GetImageFromIndex(i);
                var etat = _match.GetTileState(i);
                switch (etat)
                {
                    case TileState.Empty:
                        image.Source = "";
                        break;
                    case TileState.O:
                        image.Source = "imgO";
                        break;
                    case TileState.X: 
                        image.Source = "imgX";
                        break;
                }
            }
            if (_match.Finished())
            {
                switch (_match.Winner())
                {
                    case TileState.Empty: LblResult.Text = "Partie nulle"; break;
                    case TileState.X: LblResult.Text = "X gagne"; break;
                    case TileState.O: LblResult.Text = "O gagne"; break;
                }
            } else {
                LblResult.Text = "";
            }
        }

        private Button GetButtonFromIndex(int index)
        {
            switch (index)
            {
                case 0: return ButtonTopLeft;
                case 1: return ButtonTop;
                case 2: return ButtonTopRight;
                case 3: return ButtonCenterLeft;
                case 4: return ButtonCenter;
                case 5: return ButtonCenterRight;
                case 6: return ButtonBottomLeft;
                case 7: return ButtonBottomCenter;
                case 8: return ButtonBottomRight;
            }
            throw new System.Exception("Wrong index for button : " + index);
        }
        private Image GetImageFromIndex(int index)
        {
            switch (index)
            {
                case 0: return ImgTopLeft;
                case 1: return ImgTop;
                case 2: return ImgTopRight;
                case 3: return ImgCenterLeft;
                case 4: return ImgCenter;
                case 5: return ImgCenterRight;
                case 6: return ImgBottomLeft;
                case 7: return ImgBottomCenter;
                case 8: return ImgBottomRight;
            }
            throw new System.Exception("Wrong index for image : " + index);
        }
    }
    
}