﻿using System;

using System.Net.NetworkInformation;
using System.Net;

namespace AppServer
{
    /// <summary>
    /// C'est l'executable qui va lancer un serveur autoritaire pour morpion
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            string smartfoxhost = "app";
            string mysqlHost = "mysql";

            System.Threading.Thread.Sleep(5000);

            IPHostEntry hostEntry;
            hostEntry = Dns.GetHostEntry("app");
            if (hostEntry.AddressList.Length > 0)
            {
                smartfoxhost = hostEntry.AddressList[0].ToString();
                Console.WriteLine("smartfox host resolved to " + smartfoxhost);
            }
            hostEntry = Dns.GetHostEntry(mysqlHost);
            if (hostEntry.AddressList.Length > 0)
            {
                mysqlHost = hostEntry.AddressList[0].ToString();
                Console.WriteLine("mysql host resolved to " + mysqlHost);
            }

            MatchManager manager = new MatchManager(smartfoxhost, mysqlHost);

            while (true)
            {
                manager.Update();
                System.Threading.Thread.Sleep(10);
            }
        }
    }
}
