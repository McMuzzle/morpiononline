﻿using System;
using System.Collections.Generic;

using MySql.Data.MySqlClient;

using Sfs2X;
using Sfs2X.Core;
using Sfs2X.Entities;
using Sfs2X.Entities.Data;
using Sfs2X.Requests;

using MorpionGame;

namespace AppServer
{
    /// <summary>
    /// Une classe qui va gerer des matchs de morpions.  C'est le serveur "autoritaire" qui valide les
    /// coups des joueurs.
    /// En gros il va etre dans les rooms reseau avec les joueurs, recevoir leurs requetes de coups, 
    /// valider et renvoyer a tout le monde le coup a jouer.
    /// 
    /// todo : passer la logique smartfox ailleur?
    /// </summary>
    class MatchManager
    {
        /// <summary> la connection au serveur Smartfox</summary>
        SmartFox _smartfox = null;
        /// <summary> L'ip du serveur pour la connection</summary>
        private string _smartfoxHost = "";

        private string _mysqlHost = "";

        struct MatchDetail
        {
            public Match match;
            public Room room;
        }

        /// <summary>une liste qui contient l'etat des matchs en cours</summary>
        Dictionary<string, MatchDetail> _matchDetails = new Dictionary<string, MatchDetail>();

        public MatchManager(string smartfoxHost, string mysqlHost)
        {
            _smartfoxHost = smartfoxHost;
            _mysqlHost = mysqlHost;
            Connect();        
        }

        private void Connect()
        {
            //preparer la connenction
            _smartfox = new SmartFox();

            Sfs2X.Util.ConfigData config = new Sfs2X.Util.ConfigData();
            config.Host = _smartfoxHost;
            config.Debug = false;
            config.Zone = "Morpion";

            //branchement des events
            _smartfox.AddEventListener(SFSEvent.CONNECTION, OnConnect);
            _smartfox.AddEventListener(SFSEvent.LOGIN, OnLogin);
            _smartfox.AddEventListener(SFSEvent.LOGIN_ERROR, OnLoginError);
            _smartfox.AddEventListener(SFSEvent.ROOM_JOIN, OnRoomJoin);
            _smartfox.AddEventListener(SFSEvent.CONNECTION_LOST, OnConnectionLost);
            _smartfox.AddEventListener(SFSEvent.PUBLIC_MESSAGE, OnMessage);
            _smartfox.AddEventListener(SFSEvent.EXTENSION_RESPONSE, OnExtensionResponse);

            _smartfox.AddEventListener(SFSEvent.USER_ENTER_ROOM, OnNewUserInRoom);
            _smartfox.AddEventListener(SFSEvent.USER_EXIT_ROOM, OnUserExitInRoom);


            _smartfox.Connect(config);
        }

        /// <summary> 
        /// cette methode doit etre appelee regulierement, on gere les input et output vers le
        /// serveur et l'etat de la connection
        /// </summary>
        public void Update()
        {
            while (true)
            {
                if (null != _smartfox && !_smartfox.IsConnected && !_smartfox.IsConnecting)
                {
                    Connect();
                }
                else
                {
                    _smartfox.ProcessEvents();
                }
            }
        }

        /// <summary> event quand une tentative de connection se termine, bien ou mal </summary>
        /// <param name="evt"></param>
        public void OnConnect(BaseEvent evt)
        {
            bool success = (bool)evt.Params["success"];
            Console.WriteLine("On Connection callback got: " + success);

            _smartfox.Send(new LoginRequest("Server"));
        }

        /// <summary> event quand le client est logue avec son username</summary>
        /// <param name="evt"></param>
        public void OnLogin(BaseEvent evt)
        {
            Console.WriteLine("Login success");
            _smartfox.Send(new JoinRoomRequest("The Lobby", "", null, false));
        }

        /// <summary> event lance chaque fois qu'on entre dans une room sur le serveur</summary>
        /// <param name="evt"></param>
        public void OnRoomJoin(BaseEvent evt)
        {
            Room room = (Room)evt.Params["room"];
            Console.WriteLine("Joined : " + room.Name);
            //get room if needed 
            if (room.GroupId == "match")
            {
                if (!_matchDetails.ContainsKey(room.Name))
                {
                    _matchDetails.Add(room.Name, new MatchDetail() { match = GetMatch(room.Name), room = room });
                }
                else
                {
                    var tmp = _matchDetails[room.Name];
                    tmp.room = room;
                    _matchDetails[room.Name] = tmp;
                }
            }
        }

        /// <summary> Event leve quand une erreur survient au login</summary>
        /// <param name="evt"></param>
        void OnLoginError(BaseEvent evt)
        {
            string message = (string)evt.Params["errorMessage"];
            Console.WriteLine("Error login : " + message);
        }

        /// <summary> event de perte de connection, on relance la connection</summary>
        /// <param name="evt"></param>
        public void OnConnectionLost(BaseEvent evt)
        {
            Connect();
        }

        /// <summary> quand un utilisateur dans une room envoie un message sur le chat</summary>
        /// <param name="evt"></param>
        public void OnMessage(BaseEvent evt)
        {
            var room = (Room)evt.Params["room"];
            var user = (User)evt.Params["sender"];
            string message = (string)evt.Params["message"];
            var data = (ISFSObject)evt.Params["data"];

            Console.WriteLine("Message(" + room.Name + ") : " + message);
        }

        /// <summary> event qui indique que le serveur nous envoie une requete a traiter </summary>
        /// <param name="evt"></param>
        private void OnExtensionResponse(BaseEvent evt)
        {
            //le nom de la commande a traiter
            string cmd = (string)evt.Params["cmd"];
            //on verifie si la commande vient d'une room ou de la zone(globale)
            Room room = null;
            if (evt.Params.ContainsKey("room"))
            {
                room = (Room)evt.Params["room"];
            }
            //les parametres envoyes avec la commande
            ISFSObject data = (ISFSObject)evt.Params["params"];

            switch (cmd)
            {
                case "Play": // un joueur essais de jouer un coup, on doit valider 
                    {
                        int cell = data.GetInt("cell");
                        string playerName = data.GetUtfString("player");

                        if (playerName == null)
                            return;
                        int currentPlayer = _matchDetails[room.Name].match.CurrentPlayer;
                        if (_matchDetails.ContainsKey(room.Name))
                        {  
                            Match m = _matchDetails[room.Name].match;                            
                            if ((m.CurrentPlayer == 1 && m.Player1 == playerName) ||
                               (m.CurrentPlayer == -1 && m.Player2 == playerName))
                            {
                                if (m.CanPlayCell(cell))
                                {
                                    m.Play(cell);
                                    if (m.Finished())
                                    {
                                        RemoveMatch(m.Name);
                                    }
                                    ISFSObject playData = new SFSObject();
                                    playData.PutInt("cell", cell);
                                    ISFSObject sendData = new SFSObject();
                                    sendData.PutUtfString("cmd", "PlayConfirmation");
                                    sendData.PutSFSObject("data", playData);
                                    _smartfox.Send(new ExtensionRequest("ServerToRoom", sendData, _matchDetails[room.Name].room));
                                }
                            }
                        }
                    }
                    break;
            }
        }
        /// <summary> 
        /// event quand un joueur entre dans une room, il faut lui envoyer l'etat du match a son entree 
        /// </summary>
        /// <param name="evt"></param>
        private void OnNewUserInRoom(BaseEvent evt)
        {
            Room room = (Room)evt.Params["room"];
            User user = (User)evt.Params["user"];

            if (_matchDetails.ContainsKey(room.Name))
            {
                ISFSObject data = new SFSObject();
                ISFSObject matchData = new SFSObject();
                matchData.PutUtfString("json", _matchDetails[room.Name].match.Serialize());
                matchData.PutUtfString("name", room.Name);
                data.PutUtfString("cmd", "matchData");
                data.PutSFSObject("data", matchData);
                data.PutUtfString("username", user.Name);
                _smartfox.Send(new ExtensionRequest("ServerToUser", data, room));
            }
        }

        private void OnUserExitInRoom(BaseEvent evt)
        {
            Room room = (Room)evt.Params["room"];
            User user = (User)evt.Params["user"];
            if (room.Name != "The Lobby" && room.UserCount == 1 && user.Name != _smartfox.MySelf.Name)
                _smartfox.Send(new LeaveRoomRequest(room));
        }

        private Match GetMatch(string matchName)
        {
            int port = 3306;
            string database = "morpiononline";
            string username = "root";
            string password = "secret";

            // Connection String.
            String connString = "Server=" + _mysqlHost + ";Database=" + database
                + ";port=" + port + ";User Id=" + username + ";password=" + password;

            MySqlConnection conn = new MySqlConnection(connString);
            conn.Open();

            string sql = $"SELECT * from matchs WHERE name='{matchName}';";
            MySqlCommand cmd = new MySqlCommand(sql, conn);
            MySqlDataReader rdr = cmd.ExecuteReader();

            while (rdr.Read())
            {
                string json = rdr["json"] as string;
                if ( json == null || json == "")
                {
                    var result = new Match(matchName) { Player1 = rdr["player1"] as string , Player2 = rdr["player2"] as string };
                    conn.Close();
                    return result;
                } 
                else
                {
                    conn.Close();
                    return MatchFactory.MatchFromJSON(json) as Match;
                }                
            }
            rdr.Close();
            conn.Close();

            return null;
        }

        private void RemoveMatch(string name)
        {
            int port = 3306;
            string database = "morpiononline";
            string username = "root";
            string password = "secret";

            // Connection String.
            String connString = "Server=" + _mysqlHost + ";Database=" + database
                + ";port=" + port + ";User Id=" + username + ";password=" + password;

            MySqlConnection conn = new MySqlConnection(connString);
            conn.Open();

            string sql = $"DELETE from matchs WHERE name='{name}';";
            MySqlCommand cmd = new MySqlCommand(sql, conn);
            int nbRow = cmd.ExecuteNonQuery();

            conn.Close();
        }
    }
}
