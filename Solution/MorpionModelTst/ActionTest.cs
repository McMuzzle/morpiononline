﻿using NUnit.Framework;

namespace MorpionModelTst
{
    class ActionTest
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void LegalMoves()
        {
            //player1 wins
            MorpionGame.Match m = MorpionGame.Match.Deserialize(TestMatchDB.matchDebut);
            m.Play(0);
            Assert.IsTrue(m.Winner() == MorpionGame.TileState.Empty && !m.Finished());
            Assert.IsTrue(m.CurrentPlayer == -1);

            m.Play(1);
            Assert.IsTrue(m.CurrentPlayer == 1);
            Assert.IsFalse(m.Finished());

            m.Play(3);
            m.Play(4);
            m.Play(6);
            Assert.IsTrue(m.Finished());
            Assert.AreEqual(MorpionGame.TileState.X, m.Winner());

            //partie nulle
            m = MorpionGame.Match.Deserialize(TestMatchDB.oneMoveXLeft);
            m.Play(5);
            Assert.IsTrue(m.Finished());
            Assert.AreEqual(MorpionGame.TileState.Empty, m.Winner());
        }        
    }
}
