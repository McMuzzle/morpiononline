﻿using NUnit.Framework;

namespace MorpionModelTst
{
    class EventsTest
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void OnChangeEvents()
        {
            MorpionGame.Match m = MorpionGame.Match.Deserialize(TestMatchDB.matchDebut);
            bool changed = false;
            m.OnMatchChange += () => { changed = true; };

            m.Play(0);
            Assert.IsTrue(changed);
        }   
    }
}
