﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MorpionModelTst
{
    class TestMatchDB
    {
        public static string matchDebut = "{\"_cells\":[0,0,0,0,0,0,0,0,0],\"_currentPlayer\":1,\"_winner\":0,\"Name\":\"DefaultName\",\"Player1\":null,\"Player2\":null}";
        public static string oneMoveXLeft = "{\"_cells\":[1,-1,1,-1,-1,0,1,1,-1],\"_currentPlayer\":1,\"_winner\":0,\"Name\":\"DefaultName\",\"Player1\":null,\"Player2\":null}";
        public static string oneMoveXLeftToWin = "{\"_cells\":[1,-1,1,-1,0,-1,1,1,-1],\"_currentPlayer\":1,\"_winner\":0,\"Name\":\"DefaultName\",\"Player1\":null,\"Player2\":null}";
        public static string doneMatch = "{\"_cells\":[1,1,1,-1,-1,0,0,0,0],\"_currentPlayer\":0,\"_winner\":0,\"Name\":\"DefaultName\",\"Player1\":null,\"Player2\":null}";
    }
}
