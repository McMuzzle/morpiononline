using NUnit.Framework;

namespace MorpionModelTst
{
    /// <summary>
    /// Les tests de serialization et deserialization d'un match pour s'assurer de ne pas perdre les valeurs
    /// </summary>
    public class SerializationTest
    {


        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void Serialize1()
        {
            MorpionGame.Match m = new MorpionGame.Match();
            string data = m.Serialize();

            Assert.AreEqual(data, TestMatchDB.matchDebut);
        }

        [Test]
        public void Deserialize1()
        {
            MorpionGame.Match m = MorpionGame.Match.Deserialize(TestMatchDB.matchDebut);
            Assert.IsFalse(m.Finished());

            MorpionGame.Match m2 = MorpionGame.Match.Deserialize(TestMatchDB.oneMoveXLeft);
            Assert.IsFalse(m2.Finished());

            MorpionGame.Match m3 = MorpionGame.Match.Deserialize(TestMatchDB.doneMatch);
            Assert.IsTrue(m3.Finished());
        }
        
    }
}
