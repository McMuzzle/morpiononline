import java.sql.SQLException;

import com.smartfoxserver.v2.db.IDBManager;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSArray;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSArray;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;

public class GetAllMatchs extends BaseClientRequestHandler {

	@Override
	public void handleClientRequest(User sender, ISFSObject datas) {

		
		ISFSObject result = SFSObject.newInstance();
		SFSArray matchs = SFSArray.newInstance();		
	
		IDBManager db = getParentExtension().getParentZone().getDBManager();
		
		String getRequest = "SELECT * FROM matchs;";		
		try {
			ISFSArray matchsResult = db.executeQuery(getRequest, new Object[] {});
			for(int i = 0; i < matchsResult.size(); i++) {
				ISFSObject line = matchsResult.getSFSObject(i);
				
				SFSObject match2 = SFSObject.newInstance();
				match2.putUtfString("name", line.getUtfString("name"));
				String player1 = line.getUtfString("player1") == null ? "" : line.getUtfString("player1");
				String player2 = line.getUtfString("player2") == null ? "" : line.getUtfString("player2");
				match2.putUtfString("player1", player1);
				match2.putUtfString("player2", player2);
				String json = line.getUtfString("json") == null ? "" : line.getUtfString("json");
				match2.putUtfString("json", json);
				matchs.addSFSObject(match2);				
			}
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
						
		result.putSFSArray("matchs", matchs);
		result.putInt("id", datas.getInt("id"));
		send("MatchList", result, sender);
	}

}
