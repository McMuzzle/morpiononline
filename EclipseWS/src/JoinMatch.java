import java.sql.SQLException;

import com.smartfoxserver.v2.db.IDBManager;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSArray;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;

public class JoinMatch extends BaseClientRequestHandler {

	@Override
	public void handleClientRequest(User sender, ISFSObject data) {

		String matchName = data.getUtfString("matchname");
		
		IDBManager db = getParentExtension().getParentZone().getDBManager();
		
		String getRequest = "SELECT * FROM matchs WHERE name=?;";		
		try {
			ISFSArray matchs = db.executeQuery(getRequest, new Object[] {matchName});
			if(matchs.size() == 1) {
				ISFSObject match = matchs.getSFSObject(0);
				if(match.getUtfString("player1") == null) {
					String request = "UPDATE matchs SET player1=? WHERE name=?;";
					db.executeUpdate(request, new Object[] {sender.getName(), matchName});
					trace(sender.getName() + " join " + matchName + " as player1");
				} else if(match.getUtfString("player2") == null) {
					String request = "UPDATE matchs  SET player2=? WHERE name=?;";
					db.executeUpdate(request, new Object[] {sender.getName(), matchName});
					trace(sender.getName() + " join " + matchName + " as player2");
				} else {
					trace("Joining game with 2 player : " + match.getUtfString("player1") +" vs " + match.getUtfString("player2"));
				}
			} else {
				trace("match size : " + matchs.size());
			}
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

}
