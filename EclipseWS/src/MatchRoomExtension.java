import com.smartfoxserver.v2.extensions.SFSExtension;

public class MatchRoomExtension extends SFSExtension {

	@Override
	public void init() {
		
		addRequestHandler("Play", PlayAction.class);
		addRequestHandler("ServerToUser", ServerToUser.class);
		addRequestHandler("ServerToRoom", ServerToRoom.class);
	}

}
