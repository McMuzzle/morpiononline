import java.sql.SQLException;
import java.util.Random;

import com.smartfoxserver.v2.db.IDBManager;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;

public class CreateMatch extends BaseClientRequestHandler {

	@Override
	public void handleClientRequest(User sender, ISFSObject data) {

		IDBManager db = getParentExtension().getParentZone().getDBManager();
		
		
		String sql = "INSERT INTO `matchs` (`name`, `player1`, `player2`) VALUES (concat('match_',(select AUTO_INCREMENT from  information_schema.tables where TABLE_NAME='matchs')), ?, ?);";		
		try {
			Random r = new Random();
			if(r.nextDouble() > 0.5) {
				Object rowId = db.executeInsert(sql, new Object[] {sender.getName(), null});
				trace(rowId.toString());
			} else {
				Object rowId = db.executeInsert(sql, new Object[] {null, sender.getName()});
				trace(rowId.toString());
			}
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}

}
