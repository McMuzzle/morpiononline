import com.smartfoxserver.v2.extensions.SFSExtension;

public class MorpionOnlineExtension extends SFSExtension {

	@Override
	public void init() {
		trace("Extension chargée");
		
		addRequestHandler("GetMatchs", GetAllMatchs.class);
		addRequestHandler("JoinMatchRoom", JoinMatchRoom.class);
		addRequestHandler("CreateMatch", CreateMatch.class);
		addRequestHandler("JoinMatch", JoinMatch.class);
	}

}
