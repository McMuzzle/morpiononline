import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;

public class ServerToRoom extends BaseClientRequestHandler {

	@Override
	public void handleClientRequest(User sender, ISFSObject data) {

		if(!sender.getName().equals("Server")) {
			trace(sender.getName() + " wants to be server");		
			return;
		}
		

		for(User target : getParentExtension().getParentRoom().getUserList()) {
			send(data.getUtfString("cmd"),data.getSFSObject("data"),target);
		}
	}

}
