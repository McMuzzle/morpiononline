import com.smartfoxserver.v2.entities.Room;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;

public class PlayAction extends BaseClientRequestHandler {

	@Override
	public void handleClientRequest(User sender, ISFSObject data) {
		
		Room room = getParentExtension().getParentRoom();
		
		ISFSObject resultData = SFSObject.newInstance();
		resultData.putInt("cell", data.getInt("cell"));
		resultData.putInt("state", data.getInt("state"));
		resultData.putUtfString("player", sender.getName());
		
		User server = room.getUserByName("Server");
		
		send("Play",resultData,server);
	}
}
