import com.smartfoxserver.v2.api.CreateRoomSettings;
import com.smartfoxserver.v2.api.CreateRoomSettings.RoomExtensionSettings;
import com.smartfoxserver.v2.entities.Room;
import com.smartfoxserver.v2.entities.SFSRoomRemoveMode;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.exceptions.SFSCreateRoomException;
import com.smartfoxserver.v2.exceptions.SFSJoinRoomException;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;

public class JoinMatchRoom extends BaseClientRequestHandler {

	@Override
	public void handleClientRequest(User sender, ISFSObject data) {
		int id = data.getInt("id");
		String matchName = data.getUtfString("matchName");
		
		//create room if not exist
		Room r = getParentExtension().getParentZone().getRoomByName(matchName);
		if(null == r) {
			//create room
			RoomExtensionSettings roomSetting = new RoomExtensionSettings("MorpionOnline", "MatchRoomExtension");
			CreateRoomSettings settings = new CreateRoomSettings(); 
			settings.setName(matchName);
			settings.setGroupId("match");			
			settings.setExtension(roomSetting);
			settings.setAutoRemoveMode(SFSRoomRemoveMode.WHEN_EMPTY);
			settings.setDynamic(true);
			try {				
				r = getParentExtension().getParentZone().createRoom(settings);
			} catch (SFSCreateRoomException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		//check if room has server
		if(!r.containsUser("Server")) {
			User server = getParentExtension().getParentZone().getUserByName("Server");
			try {
				getApi().joinRoom(server, r,"",false,null,true,false);
			} catch (SFSJoinRoomException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
				
		try {
			getApi().joinRoom(sender, r,"",false,null,true,false);
		} catch (SFSJoinRoomException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
	
		SFSObject result = SFSObject.newInstance();
		result.putInt("id", id);		
		send("JoinMatchRoom", result, sender);
	}

}
